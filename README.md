## Update all Servers from a list
Bash script to update multiple servers, running Debian/Ubuntu or CentOS/RHEL, using only SSH and sudo.

### Description

Dieses Bash Script dient dazu mehrere Server aktuell zu halten, ohne einzeln auf jeden Server zugreifen zu müssen. Es nutzt es einen unpriviligierten User, dem wir per sudo ausschließlich die Rechte zum updaten gewähren. Der User verbindet sich via ssh und public key an den servern an.  

#### Usage
##### Configure your target server:  

Install sudo and add the new updateuser, don't forget the password:  

Debian/Ubuntu:
```bash
root@debian ~# apt install sudo
root@debian ~# adduser updateuser
root@debian ~# echo -e "updateuser ALL=(root) NOPASSWD: /usr/bin/apt-get update\nupdateuser ALL=(root) NOPASSWD: /usr/bin/apt-get -y upgrade" > /etc/sudoers.d/update-debian

```

CentOS/RHEL:
```bash
root@centos ~# yum -y install sudo
root@centos ~# adduser updateuser
root@centos ~# passwd updateuser
root@centos ~# echo -e "updateuser ALL=(root) NOPASSWD: /usr/bin/yum -y update" > /etc/sudoers.d/update-centos

```  

##### Configure your Workstation:
```bash
you@workstation % git clone https://git.buttonmonkeys.de/andre/update-all-servers.git
you@workstation % cd update-all-servers
```
Add the Servers you configured to your server.list:
```
# List all servers you want to update.
#
# syntax:
# IP/Hostname port OS
#
# OS types are: 'CentOS' 'RHEL' 'Debian' 'Ubuntu'
#
# 127.0.0.1 22 Debian
# DOMAIN.COM 22 CentOS
# DOMAIN.COM 2022 RHEL
192.168.1.10 22 Debian
192.168.1.11 22 CentOS
```  
Copy your  public ssh-key to the servers, you need the passwords from your updateuser:
```bash
you@workstation % ssh-copy-id -i ~/.ssh/id_ed25519.pub updateuser@debian
you@workstation % ssh-copy-id -i ~/.ssh/id_ed25519.pub updateuser@centos
```
If you don't have one, run:
```bash
ssh-keygen -t ed25519
```

and finaly run the updater script:
```bash
sh updateall.sh
```
