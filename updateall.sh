#!/bin/bash
 
workdir=$PWD
logfile=$workdir/updaterun.run
listfile=$workdir/server.list
 
(
cd $workdir/
if [ -a $logfile ] ; then
    echo ""
    echo "Script Is allready Running - check if $logfile exists" 
    echo ""
    exit;
fi

echo "************************************************************"
echo "**     Starting updates..." $(date +%Y%m%d-%H:%M:%S)     "**"
echo "************************************************************"

grep -v '^#.*' $listfile | while read line
do
    dist=$(echo $line | awk '{print $NF}')
    case $dist in
        'CentOS'|'RHEL')
            server=$(echo $line | awk '{print($(NF-2))}')
            port=$(echo $line | awk '{print($(NF-1))}')
            echo ""
            echo "************************************************************"
            echo "**              " $server " " $dist "                     **"
            echo "************************************************************"
            echo ""
            ssh -n -l updateuser -p $port $server 'sudo yum -y update'
        ;;
        'Debian'|'Ubuntu')
            server=$(echo $line | awk '{print($(NF-2))}')
            port=$(echo $line | awk '{print($(NF-1))}')
            echo ""
            echo "************************************************************"
            echo "**              " $server " " $dist "                     **"
            echo "************************************************************"
            echo ""
            ssh -n -l updateuser -p $port $server 'sudo apt-get update'
            ssh -n -l updateuser -p $port $server 'sudo apt-get -y upgrade'
        ;;
        *)
            echo "************************************************************"
            echo "*     Unknown Linux Distribution                          **"
            echo "************************************************************"
            echo ""
        ;;
    esac
done
echo ""
echo "************************************************************"
echo "**      All Servers Updated, search the log to see        **"
echo "**             if manual work is needed.                  **"
echo "************************************************************"
echo ""
) 2>&1 | tee -a $logfile
mv $logfile $workdir/logs/update-$(date +%Y%m%d-%H:%M:%S)

